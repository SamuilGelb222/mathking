﻿using System;

[Serializable]
public class Quests
{
    public Question[] quests;
    public string name;
}

[Serializable]
public class Question
{
    public string question;
    public File file;
    public Ansser[] asnwers;
}

[Serializable]
public class Ansser
{
    public string content;
    public File file;
    public bool isRight;
}

[Serializable]
public class File
{
    public string link;
    public string name;
}