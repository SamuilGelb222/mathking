﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.Collections;
using UnityEngine.Networking;

public class GameUIController : MonoBehaviour
{
    public Quests _quest;
    public TextMeshProUGUI _questName;
    public TextMeshProUGUI _timer;
    public List<Image> _stars;

    public int _startScore;
    public int _currentScore;

    public TextMeshProUGUI _question;
    public Image _questImg;
    public List<AnswerUI> _questButtons = new List<AnswerUI>();

    private int _currentQuestion = 0;

    [Serializable]
    public class AnswerUI
    {
        public TextMeshProUGUI _text;
        public Image _image;
    }

    public void Update()
    {
        if (_currentScore > 0)
        {
            _currentScore -= (int)(Time.deltaTime * 1000);
            _timer.text = $"{_currentScore}";
        }
        else
        {
            _timer.text = $"{0}";
            Time.timeScale = 0;
        }
    }

    public void OnButtonClick(int id)
    {
        if (_quest.quests[_currentQuestion].asnwers[id].isRight)
        {
            Debug.Log("Right");
            _currentQuestion++;
            if (_currentQuestion >= _quest.quests.Length)
            {
                EndGame();
            }
            else
            {
                DrawQuest();
            }
        }
        else
        {
            Debug.LogError("Wrong data");
        }
    }

    private void EndGame()
    {
        Time.timeScale = 0;
    }

    public void Awake()
    {
        _quest = null;
        string path = Path.Combine(Application.persistentDataPath, "q.txt");
        string content = System.IO.File.ReadAllText(path);
        _quest = JsonConvert.DeserializeObject<Quests>(content);
        DrawQuest();
        _questName.text = _quest.name;
        _startScore = _quest.quests.Length * 10000;
        _currentScore = _startScore;
    }

    private void DrawQuest()
    {
        _question.text = _quest.quests[_currentQuestion].question;
        if (_quest.quests[_currentQuestion].file != null && _quest.quests[_currentQuestion].file.link != null && _quest.quests[_currentQuestion].file.link != "")
        {
            _question.rectTransform.sizeDelta = new Vector2(1080, 540);
            _questImg.gameObject.SetActive(true);
            StartCoroutine(GetImage(_quest.quests[_currentQuestion].file.link, _questImg));
        }
        else
        {
            _question.rectTransform.sizeDelta = new Vector2(1080, 1080);
        }
        for (int i = 0; i < _questButtons.Count; i++)
        {
            _questButtons[i]._text.text = _quest.quests[_currentQuestion].asnwers[i].content;
            if (_quest.quests[_currentQuestion].asnwers[i].file != null
                && _quest.quests[_currentQuestion].asnwers[i].file.link != null
                && _quest.quests[_currentQuestion].asnwers[i].file.link != "")
            {
                _questButtons[i]._image.gameObject.SetActive(true);
                StartCoroutine(GetImage(_quest.quests[_currentQuestion].asnwers[i].file.link, _questButtons[i]._image));
                _questButtons[i]._text.rectTransform.sizeDelta = new Vector2(_questButtons[i]._text.rectTransform.sizeDelta.x / 2, _questButtons[i]._text.rectTransform.sizeDelta.y);
            }
            else
            {
                _questButtons[i]._text.rectTransform.sizeDelta = new Vector2(540, 310);
            }
        }
    }

    public IEnumerator GetImage(string link, Image toImage)
    {
        Texture2D texture;
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(link))
        {
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                texture = DownloadHandlerTexture.GetContent(uwr);
                toImage.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            }
        }
    }
}